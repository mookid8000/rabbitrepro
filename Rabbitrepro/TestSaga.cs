﻿using System;
using System.Threading.Tasks;
using Rebus.Bus;
using Rebus.Handlers;
using Rebus.Sagas;
using Serilog;

namespace Rabbitrepro
{
    public class TestSaga : Saga<TestData>, IAmInitiatedBy<TestMessage>, IHandleMessages<TestMessage2>
    {
        static readonly ILogger Logger = Log.ForContext<TestSaga>();
        readonly IBus _bus;

        public TestSaga(IBus bus)
        {
            _bus = bus;
        }

        protected override void CorrelateMessages(ICorrelationConfig<TestData> config)
        {
            config.Correlate<TestMessage>(m => m.Id, s => s.TestId);
            config.Correlate<TestMessage2>(m => m.Id, s => s.TestId);
        }

        public async Task Handle(TestMessage message)
        {
            await _bus.Defer(TimeSpan.FromSeconds(1), new TestMessage2 { Id = message.Id });
            
            Data.TestId = message.Id;
            
            Logger.Information("testmessage1 handled");
        }

        public async Task Handle(TestMessage2 message)
        {
            MarkAsComplete();
            
            await Task.CompletedTask;
            
            Logger.Information("testmessage2 handled");
        }
    }

    public class TestData : SagaData
    {
        public Guid TestId { get; set; }
    }
}