﻿namespace Rabbitrepro
{
    static class QueueNames
    {
        public const string Queue = "my-queue";
        public const string ErrorQueue = "my-error-queue";
    }

    static class ExchangeNames
    {
        public const string Direct = "CustomRebusDirect";
        
        public const string Topics = "CustomRebusTopics";
    }
}