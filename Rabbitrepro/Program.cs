﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rebus.Bus;
using Rebus.Config;
using Rebus.Handlers;
using Rebus.Retry.Simple;
using Rebus.Routing.TypeBased;
using Rebus.ServiceProvider;
using Serilog;
// ReSharper disable ArgumentsStyleNamedExpression
// ReSharper disable ArgumentsStyleLiteral
// ReSharper disable RedundantArgumentDefaultValue

namespace Rabbitrepro
{
    class Program
    {
        static Program() =>
            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole()
                .MinimumLevel.Information()
                .CreateLogger();

        static IConfiguration InitializeConfiguration() =>
            new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

        static IConfiguration Configuration { get; } = InitializeConfiguration();

        static async Task Main()
        {
            var rebusConnection = Configuration["rebus:sql"];
            var rebusThreads = int.Parse(Configuration["rebus:threads"]);

            var serviceCollection = new ServiceCollection();

            serviceCollection.AddRebus(configure => configure
                .Options(o => o.SetBusName("Test"))
                .Options(o => o.LogPipeline(verbose: true))
                .Logging(l => l.Serilog())
                .Transport(t => t.UseRabbitMq(Configuration["rebus:rabbit"], QueueNames.Queue)
                    .ExchangeNames(ExchangeNames.Direct, ExchangeNames.Topics)
                    .Prefetch(50))
                .Timeouts(t => t.StoreInSqlServer(rebusConnection, "RebusTimeout", true))
                .Sagas(s => s.StoreInSqlServer(rebusConnection, "RebusSaga", "RebusSagaIndex", true))
                .Options(o => o.SetMaxParallelism(rebusThreads))
                .Options(o => o.SimpleRetryStrategy(
                    errorQueueAddress: QueueNames.ErrorQueue,
                    maxDeliveryAttempts: 1,
                    secondLevelRetriesEnabled: false)
                )
                .Routing(r => r.TypeBased()
                    .MapAssemblyOf<Program>(QueueNames.Queue)
                    .MapFallback(QueueNames.ErrorQueue))
            );

            serviceCollection.AddTransient<IHandleMessages<TestMessage>, TestSaga>();
            serviceCollection.AddTransient<IHandleMessages<TestMessage2>, TestSaga>();

            var provider = serviceCollection.BuildServiceProvider();

            using var bus = provider.GetRequiredService<IBus>();

            //await bus.Send(new TestMessage { Id = Guid.NewGuid() });

            for (var i = 1; i <= 1000; i++)
            {
                await bus.Send(new TestMessage { Id = Guid.NewGuid() });
            }

            Console.WriteLine("Press ENTER to quit");
            Console.ReadLine();
        }
    }
}
